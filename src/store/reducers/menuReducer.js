import * as actionTypes from "../actions/action";
const initialState = {

    menu: [],
    request: false,
    error: false,
    priceList:{}

};
const getFullPrice=(data)=>{
    let tmpList={};
    for (let i in data){
        tmpList[data[i].name]=data[i].price;
    }
    return tmpList;

};
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.DATA_REQUEST:
            return {...state, request: true, error: false};
        case actionTypes.DATA_SUCCESS:{
            return {...state, menu: action.data, request: false, error: false,priceList:getFullPrice(action.data)};}
        case actionTypes.DATA_ERROR:
            return {...state, request: false, error: true};

        default:
            return state
    }
};
export default reducer;