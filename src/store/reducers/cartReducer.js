import * as actionTypes from "../actions/cartAction";
const initialState = {

    cart: {},
    total: {},
    modal: false,
    loading: false

};
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LOADING:
            return {...state, cart: {...state.cart}, total: {...state.total}, loading: true};
        case actionTypes.RESET_CART:
            return {loading:false,cart: {}, total: {}, modal: false};
        case actionTypes.SHOW_MODAL:
            return {...state, modal: true};
        case actionTypes.HIDE_MODAL:
            return {...state, modal: false};
        case actionTypes.ADD_TO_CART: {
            console.log({...state.cart});
            if (action.element in state.cart) {
                return {...state, cart: {...state.cart, [action.element]: state.cart[action.element] + 1}}
            }
            else return {...state, cart: {...state.cart, [action.element]: state.cart[action.element] = 1}};
        }
        case actionTypes.REMOVE_FROM_CART: {
            console.log(action.element in state.cart, action.element);
            if (action.element in state.cart && state.cart[action.element] - 1 > 0) {
                return {...state, cart: {...state.cart, [action.element]: state.cart[action.element] - 1}}
            }
            else {
                let tmp = {...state, cart: {...state.cart, [action.element]: state.cart[action.element] - 1}};
                delete(tmp.cart[action.element]);
                return tmp
            }

        }


        default:
            return state
    }
};
export default reducer;