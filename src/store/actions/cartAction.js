import axios from "../../axios-orders";

export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';
export const RESET_CART = 'RESET_CART';
export const LOADING='LOADING'

export const loading=()=>{
    return {type:LOADING}
};
export const addToCart = (element, price) => {
    return {type: ADD_TO_CART, element, price}
};
export const removeFromCart = (element, price) => {
    return {type: REMOVE_FROM_CART, element, price}
};

export const resetCart = () => {
    return {type: RESET_CART}
};

export const showModal = () => {
    return {type: SHOW_MODAL}
};
export const hideModal = () => {
    return {type: HIDE_MODAL}
};

export const placeOrder = (order) => {

    return (dispatch, getState) => {
        dispatch(loading());
        axios.post('/savedOrders.json', order).then(() => {
            dispatch(resetCart())


        })
    }
};