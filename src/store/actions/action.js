import axios from '../../axios-orders';

export const DATA_REQUEST = 'DATA_REQUEST';
export const DATA_SUCCESS = 'DATA_SUCCESS';
export const DATA_ERROR = 'DATA_ERROR';



export const dataRequest = () => {
    return {type: DATA_REQUEST}
};
export const dataSuccess = (data) => {
    return {type: DATA_SUCCESS, data}
};

export const dataError = () => {
    return {type: DATA_ERROR}
};

export const getMenu = () => {

    return (dispatch, getState) => {
        dispatch(dataRequest());
        axios.get('/menu.json').then(response => {
            dispatch(dataSuccess(response.data));

        }, error => {
            dispatch(dataError())
        })
    }
};



