import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";

import "./index.css";
import {BrowserRouter} from "react-router-dom";

import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import menuReducer from "./store/reducers/menuReducer";
import cartReducer from "./store/reducers/cartReducer";

const rootReducer = combineReducers({
    menu: menuReducer,
    cart: cartReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

const app = <Provider store={store}>
    <BrowserRouter>
        <App />
    </BrowserRouter>
</Provider>;

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
