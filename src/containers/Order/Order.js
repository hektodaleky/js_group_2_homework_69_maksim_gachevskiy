import React, {Component, Fragment} from "react";
import {connect} from "react-redux";
import "./Order.css";
import {placeOrder} from "../../store/actions/cartAction";
import Spinner from "../../components/UI/Spinner/Spinner";
class Order extends Component {
    state = {
        name: "",
        phone: "",
        address: ""
    };
    changeInfo = (event, name) => {
        this.setState({[name]: event.target.value})
    };

    orderHandler = event => {
        event.preventDefault();

        const order = {
            ingredients: this.props.cart,
            customer: {
                name: this.state.name,
                address: this.state.address,
                phone: this.state.phone,
            }
        };
        this.props.onPlaceOrder(order);
        this.setState({
            name: "",
            phone: "",
            address: ""
        });

    };

    render() {
        let totalPrice = 150;
        if (this.props.loading)
            return <Spinner/>
        else
            return (
                <Fragment>
                    <div className="Order">{
                        Object.keys(this.props.cart).map((elem, i) => {
                            totalPrice += this.props.priceList[elem];
                            return (
                                <p key={i}>{`${[elem]} : ${this.props.cart[elem]} Стоимость ${this.props.priceList[elem]}`}</p>)
                        })




                    }

                    </div>
                    <p>{`Total: ${totalPrice}`}</p>
                    <div className="Order">
                        <input type="tel" placeholder="Phone" value={this.state.phone} onChange={(event) => {
                            this.changeInfo(event, "phone")
                        }}/>
                        <input placeholder="Name" value={this.state.name} onChange={(event) => {
                            this.changeInfo(event, "name")
                        }}/>
                        <input placeholder="Address" value={this.state.address} onChange={(event) => {
                            this.changeInfo(event, "address")
                        }}/>
                        <button onClick={this.orderHandler}>Создать заказ</button>

                    </div>
                </Fragment>
            )
    }
}
;
const mapStateToProps = state => {
    return {
        cart: state.cart.cart,
        priceList: state.menu.priceList,
        loading: state.cart.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onPlaceOrder: (order) => dispatch(placeOrder(order)),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Order);