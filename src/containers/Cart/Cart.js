import React, {Component} from "react";
import {connect} from "react-redux";
import CartOrder from "../../components/CartOrder/CartOrder";
import "./Cart.css";
import CheckOut from "../../components/CheckOut/CheckOut";
import {removeFromCart} from "../../store/actions/cartAction";
class Cart extends Component {

    render() {


        let total = 150;


        return (<div className="Cart">{Object.keys(this.props.cart).map((elem, i) => {
            total += this.props.price[elem] * this.props.cart[elem];


            return <CartOrder click={() => this.props.removeCart(elem)} key={i} name={elem}
                              count={this.props.cart[elem]}
                              totalPrice={this.props.price[elem] * this.props.cart[elem]}/>
        })}

            <CheckOut click={this.props.clickShow} delivery={150} total={total}/>
        </div>);
    }
}
;


const mapStateToProps = state => {
    return {
        cart: state.cart.cart
    }
};

const mapDispatchToProps = dispatch => {
    return {
        removeCart: (element) => dispatch(removeFromCart(element))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);