import React, {Component} from "react";
import {connect} from "react-redux";
import Item from "../../components/Item/Item";
import {getMenu} from "../../store/actions/action";
import "./Menu.css";
import Spinner from "../../components/UI/Spinner/Spinner";
import {addToCart, hideModal, showModal} from "../../store/actions/cartAction";
import Cart from "../Cart/Cart";
import Modal from "../../components/UI/Modal/Modal";
import Order from "../Order/Order";

class Menu extends Component {


    componentDidMount() {
        this.props.getMenu()
    }

    render() {

        let objectItems = [];
        const price = {};
        if (this.props.request)
            objectItems = <Spinner/>;
        else (
            Object.keys(this.props.menu).map((item, i) => {
                let oneItem = this.props.menu[item];
                price[oneItem.name] = oneItem.price;
                objectItems.push(<Item key={i}
                                       url={oneItem.img}
                                       name={oneItem.name}
                                       price={oneItem.price}
                                       click={() => {
                                           this.props.addToCart(oneItem.name)
                                       }}/>);
                return null;
            }));
        return (
            <div className="TableMenu">
                <Modal show={this.props.modal} clickHide={this.props.hideMod}>
                    <Order/>
                </Modal>
                <div className="Menu">
                    {objectItems}</div>
                <Cart clickShow={this.props.showMod} price={price}/>
            </div>
        );
    }

}


const mapStateToProps = state => {
    return {
        menu: state.menu.menu,
        request: state.menu.request,
        error: state.menu.request,
        modal:state.cart.modal
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getMenu: () => dispatch(getMenu()),
        addToCart: (element) => dispatch(addToCart(element)),
        showMod:()=>dispatch(showModal()),
        hideMod:()=>dispatch(hideModal())
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Menu);