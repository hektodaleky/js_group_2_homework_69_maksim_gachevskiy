import React from 'react';
import './CartOrder.css'
const CartOrder=props=>{
    return (<div className="CartOrder" onClick={props.click}><h6>{props.name}</h6><p>{"x"+props.count}</p><i>{props.totalPrice}</i></div>)
};
export default CartOrder;