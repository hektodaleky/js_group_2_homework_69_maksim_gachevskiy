import React from "react";
import "./Item.css";
const Item = props => {
    return (
        <div className="Item">
            <div className="first">
                <img src={props.url} alt={props.name}/>
            </div>
            <div className="second">
                <h5 className="name">{props.name}</h5>
                <p className="price">{`${props.price} KGS`}</p>
                <button onClick={props.click}>Add to cart</button>
            </div>
        </div>
    )
};
export default Item;