import React from "react";
import "./CheckOut.css";
const CheckOut = props => {

    if (props.total > props.delivery)
        return (
            <div className="CheckOut">
                <p>{`Доставка ${props.delivery}`}</p>
                <p>{`Итого ${props.total}`}</p>
                <button onClick={props.click}>Place Order</button>
            </div>);
    else return (
        <div className="CheckOut">
            <h1>Сделайте заказ</h1>
        </div>
    )

};
export default CheckOut;