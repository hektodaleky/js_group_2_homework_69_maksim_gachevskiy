import React, {Component} from "react";
import "./App.css";
import Menu from "./containers/Menu/Menu";

class App extends Component {

    render() {
        return (
            <Menu/>
        );
    }
}

export default App;
